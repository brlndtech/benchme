/**
 * @file 
 * @author Thibault LOUAPRE, Geoffrey SAUVAGEOT
 * @version 1.0
 * @brief Fichier des fonctions de tri par séléction
 * @date 26-09-2019
 */
#include <stdio.h>
#include <stdlib.h>
/** 
 * @brief Trie un tableau par ordre croissant (par méthode tri selection)
 * @param float tab[] : tableau à trier
 * @param int taille : taille du tableau
 * @return tab[] la tableau trié 
 * @since Version 1.0 
 */
float* triSelectionAsc(float tab[], int taille) 
{
    for (int i=0; i < taille; i++) 
    {
        int indiceMini = i;
        float mini = tab[i];
        
        for (int j=i+1; j<taille; j++) 
        {
            if (mini > tab[j]) 
            {    
                indiceMini = j;
                mini = tab[j];
            }
        }
        
        float tmp = tab[i]; 
        tab[i] = mini;
        tab[indiceMini] = tmp;
    }

    return tab;
}

/** 
 * @brief Trie un tableau par ordre décroissant (par méthode tri selection)
 * @param float tab[] : tableau à trier
 * @param int taille : taille du tableau
 * @return tab[] la tableau trié 
 * @since Version 1.0 
 */
float* triSelectionDesc(float tab[], int taille) 
{
    for (int i=taille-1; i > 0; i--) 
    {
        int indiceMax = i;
        float max = tab[i];
        
        for (int j=i-1; j>=0; j--) 
        {
            if (max > tab[j]) 
            {
                indiceMax = j;
                max = tab[j];
            }
        }
        
        float tmp = tab[i]; 
        tab[i] = max;
        tab[indiceMax] = tmp;
    }

    return tab;
}