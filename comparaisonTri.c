/**
 * @file 
 * @author Thibault LOUAPRE, Geoffrey SAUVAGEOT
 * @version 1.0
 * @brief Fichier ComparaisonTri, réalisation du benchmark + exportation en format .csv des res 
 * @date 26-09-2019
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include<stdio.h>
#include<string.h>

void tableauAleatoire(float val[],int lenght)
{ 
    for(int i=0;i<lenght;i++){ 
        //printf("tabAleatoire: i = %d\n",i);        
        val[i]=(float)rand()/((float)RAND_MAX/1000000); 
    }
}

void benchmarkTris () 
{
    float tableau[5] = { 74.25, 82.42, 26.57, 6.23, 19.32 };
    clock_t start, end;
    double cpu_time_used;

    //génère un nombre aléatoire entre 1 et 15
    int size;
    size = (int) rand() % 20000 + 30000;
    printf("%d\n",size);

    float n;
    
    //créer un tableau de float de la taille générée précédemment
    float tab[size];

    for (int i = 0; i < size; i++) 
    {
        n = rand() % 1000 + 1;
        n = n/10;
        tab[i] = n;
    }

    start = clock();
    triInsertionAsc(tab, size);
    end = clock();
    double insertionTime1 = ((double) (end - start)) / CLOCKS_PER_SEC;
    start = clock();
    triInsertionAsc(tab, size);
    end = clock();
    double insertionTime2 = ((double) (end - start)) / CLOCKS_PER_SEC;
    start = clock();
    triInsertionAsc(tab, size);
    end = clock();
    double insertionTime3 = ((double) (end - start)) / CLOCKS_PER_SEC;
    
    start = clock();
    triBulleAsc(tab, size);
    end = clock();
    double bulleTime1 = ((double) (end - start)) / CLOCKS_PER_SEC;
    start = clock();
    triBulleAsc(tab, size);
    end = clock();
    double bulleTime2 = ((double) (end - start)) / CLOCKS_PER_SEC;
    start = clock();
    triBulleAsc(tab, size);
    end = clock();
    double bulleTime3 = ((double) (end - start)) / CLOCKS_PER_SEC;
 
    start = clock();
    triSelectionAsc(tab, size);
    end = clock();
    double selectionTime1 = ((double) (end - start)) / CLOCKS_PER_SEC;
    start = clock();
    triSelectionAsc(tab, size);
    end = clock();
    double selectionTime2 = ((double) (end - start)) / CLOCKS_PER_SEC;
    start = clock();
    triSelectionAsc(tab, size);
    end = clock();
    double selectionTime3 = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("Trie bulle     : %f, %f, %f\n", bulleTime1, bulleTime2, bulleTime3);
    printf("Trie sélection : %f, %f, %f\n", selectionTime1, selectionTime2, selectionTime3);
    printf("Trie insertion : %f, %f, %f\n", insertionTime1, insertionTime2, insertionTime3); 
    
    float moyenneBulle = (bulleTime1 + bulleTime2 + bulleTime3) / 3;
    float moyenneSelection = (selectionTime1 + selectionTime2 + selectionTime3) / 3;
    float moyenneInsertion = (insertionTime1 + insertionTime2 + insertionTime3) / 3;
    
    char* filename = "resultat-du-benchmark.csv";
    
    printf("\n Le fichier .csv est en cours de création %s",filename);
 
    FILE *fp;

    fp=fopen(filename,"w+");
    fprintf(fp,";                                Bulle;    Selection;    Insertion\n");
    fprintf(fp, "Nombre d'elements tableau; %d; %d; %d\n", size, size, size);
    fprintf(fp, "Premier test ; %f; %f; %f\n", bulleTime1, selectionTime1, insertionTime1);
    fprintf(fp, "Deuxième test ; %f; %f; %f\n", bulleTime2, selectionTime2, insertionTime2);
    fprintf(fp, "Troisième test ; %f; %f; %f\n", bulleTime3, selectionTime3, insertionTime3);
    fprintf(fp, "AVG (moyenne) ; %f; %f; %f\n", moyenneBulle, moyenneSelection, moyenneInsertion);

    fclose(fp);

    printf("\n%sfile a été créé",filename);
}