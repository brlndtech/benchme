/**
 * @file 
 * @author Thibault LOUAPRE, Geoffrey SAUVAGEOT
 * @version 1.0
 * @brief Fichier des fonctions de tri par insertion
 * @date 26-09-2019
 */
#include <stdio.h>
#include <stdlib.h>
/** 
 * @brief Trie un tableau par ordre croissant (par méthode tri insertion)
 * @param float tab[] : tableau à trier
 * @param int size (taille) : size (taille) du tableau
 * @return tab[] la tableau trié 
 * @since Version 1.0 
 */
float* triInsertionAsc(float tab[], int size) 
{
    for (int i=1; i < size; i++) 
    {
        while (tab[i] < tab[i-1]) 
        {
            float tmp = tab[i-1];
            tab[i-1] = tab[i];
            tab[i] = tmp;
            i--;
        }
    }

    return tab;
}

/** 
 * @brief Trie un tableau par ordre décroissant (par méthode tri insertion)
 * @param float tab[] : tableau à trier
 * @param int size (taille) : size (taille) du tableau
 * @return tab[] la tableau trié 
 * @since Version 1.0 
 */
float* triInsertionDesc(float tab[], int size) 
{
    for (int i=size-1; i > 0; i--) 
    {
        while (tab[i-1] < tab[i]) 
        {
            float tmp = tab[i];
            tab[i] = tab[i-1];
            tab[i-1] = tmp;
            i++;
        }
    }
    
    return tab;
}
