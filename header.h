/**
 * @file 
 * @author Thibault LOUAPRE, Geoffrey SAUVAGEOT
 * @version 1.0
 * @brief Fichier Header, définition des signatures de fonction
 * @date 26-09-2019
 */

#ifndef HEADER_H
#define HEADER_H

float* triSelectionAsc(float*, int); 

float* triSelectionDesc(float*, int); 

float* triInsertionAsc(float*, int); 

float* triInsertionDesc(float*, int); 

float* triBulleAsc(float*, int); 

float* triBulleDesc(float*, int); 

void benchmarkTris() ;

void tableauAleatoire(float*,int);



#endif /* HEADER_H */


